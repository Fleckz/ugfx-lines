A demo lines game using µGFX ( a.k.a. uGFX ) and ChibiOS on STM32F4xx (by adjusting/changing board files should work on other Hardware too, not tested though), SSD1289 display and ADS7843 touch.

**NOTE: Please, edit Makefile, change CHIBIOS and GFXLIB  path accordingly.**

[Screenshot](https://bitbucket.org/Fleckz/ugfx-lines/src/1c3ba62fec79e1c0bb14ec185365fec295443553/lines_small.png?at=master)