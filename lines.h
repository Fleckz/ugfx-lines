#ifndef _LINES_H_
#define _LINES_H_

#define LINES_CELL_WIDTH       26
#define LINES_CELL_HEIGHT      26
#define LINES_BALL_RADIUS      11
#define LINES_FIELD            9

void linesInit(void);
void linesStart(void);

#endif  /* _LINES_H_ */