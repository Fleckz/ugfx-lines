#include "gfx.h"
#include "ch.h"
#include "hal.h"
#include "lines.h"

static const char *coeffs = "A\352\204=\020\212\023\270U\341s\301\025\270D\270\262\232\261\275\372\017\253CH\v"; // got from gdb :D
static void mysave(uint16_t instance, const uint8_t *calbuf, size_t sz);
static const char *myload(uint16_t instance);

int main(void) {
  gfxInit(); // WARNING: this cmd resets periph clocks...
  ginputSetMouseCalibrationRoutines(0, mysave, myload, FALSE);
  ginputGetMouse(0);
  linesInit();
  // linesStart() will return when game is over, so we restart - 10 seconds after game over! :)
  while (TRUE) {
    linesStart();
    gfxSleepMilliseconds(10000);
  }
}

static void mysave(uint16_t instance, const uint8_t *calbuf, size_t size) {
  (void)instance;
  (void)calbuf;
  (void)size;
  return;
}

static const char *myload(uint16_t instance) {
  (void)instance;
  return coeffs;
}
