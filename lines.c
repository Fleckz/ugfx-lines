#include "gfx.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "ch.h"
#include "hal.h"
#include "lines.h"

GEventMouse     ev;

// A* algo path finding struct type
typedef struct {          // Node properties
  uint8_t nColor;         // Node color
  unsigned int nG;        // Node G value
  unsigned int nH;        // Node H value
  unsigned int nF;        // Node F value
  uint8_t nParrent;       // Node parrent value (0 = no parrent, 1 = up, 2 = right, 3 = down and 4 = left)
  uint8_t nStatus;        // Node status (Normal (0), Open (1) or Closed (2))
} nodeProps;

typedef struct {          // Struct stores two coords and a counter
  int16_t sY;             // Source Y
  int16_t sX;             // Source X
  int16_t tY;             // Source Y
  int16_t tX;             // Source X
  int16_t c;              // Count
} pointCoords;

nodeProps             linesField[LINES_FIELD][LINES_FIELD];   // main lines field array
int16_t               linesOpenMin[2];                        // [y (0) and x (1) number]
pointCoords           linesPath;                              // From/To points for moving ball
color_t               linesBallsColors[] = {Blue, Cyan, Gray, Lime, Pink, Red, Yellow};
const char            *linesBalls[] = {"ball_blue.bmp","ball_cyan.bmp","ball_gray.bmp","ball_green.bmp","ball_pink.bmp","ball_red.bmp","ball_yellow.bmp"};
int16_t               linesEmptyNodes;                        //Empty node counter
uint8_t               linesNextBalls[3];                      //Colors for next three balls
bool_t                linesGameOver                           = false;
unsigned long         linesScore = 0;
static gdispImage     linesBallImage;
int                   linesMessageTimeout                     = 2000; // "Can't move there!" message timeout
systime_t             linesPreviousMessageTime                = 0;
bool_t                linesMessageShown                       = false;


// static void initRng(void) { //STM32 RNG hardware init
//   rccEnableAHB2(RCC_AHB2ENR_RNGEN, 0);
//   RNG->CR |= RNG_CR_RNGEN;
// }

static void initRng(void) {
  srand(gfxSystemTicks());
}

// static uint32_t randomInt(uint32_t max) { //getting random number from STM32 hardware RNG
//   static uint32_t new_value=0;
//   while ((RNG->SR & RNG_SR_DRDY) == 0) { }
//   new_value=RNG->DR % max;
//   return new_value;
// }

static uint32_t randomInt(uint32_t max) {
  return rand() % max;
}

static int uitoa(unsigned int value, char * buf, int max) {
  int n = 0;
  int i = 0;
  int tmp = 0;

  if (!buf) return -3;
  if (2 > max) return -4;
  i=1;
  tmp = value;
  if (0 > tmp) {
    tmp *= -1;
    i++;
  }
  for (;;) {
    tmp /= 10;
    if (0 >= tmp) break;
    i++;
  }
  if (i >= max) {
    buf[0] = '?';
    buf[1] = 0x0;
    return 2;
  }
  n = i;
  tmp = value;
  if (0 > tmp) {
    tmp *= -1;
  }
  buf[i--] = 0x0;
  for (;;) {
    buf[i--] = (tmp % 10) + '0';
    tmp /= 10;
    if (0 >= tmp) break;
  }
  if (-1 != i) {
    buf[i--] = '-';
  }
  return n;
}

static unsigned int steps(int fromX, int fromY, int toX, int toY) {
  unsigned int ret = 0;
  if (fromX < toX) ret = toX-fromX; else ret = fromX-toX;
  if (fromY < toY) ret += toY-fromY; else ret += fromY-toY;
  return ret;
}

static void getOpenMinNode(void) {
  unsigned int i,j, min;
  min = 9999;
  linesOpenMin[0] = -1;
  linesOpenMin[1] = -1;
  for (i = 0; i < LINES_FIELD; i++) {
    for (j = 0; j < LINES_FIELD; j++) {
      if (linesField[i][j].nStatus == 1 && linesField[i][j].nF < min) {
        linesOpenMin[0] = i;
        linesOpenMin[1] = j;
        min = linesField[i][j].nF;
      }
    }
  }
}

static bool_t lookAround(int fromY, int fromX, int toX, int toY) {
  if (fromY == toY && fromX == toX) return true;
  bool_t ret = false;
  if (fromX+1 != LINES_FIELD) { // Look at the right
    if (linesField[fromY][fromX+1].nColor == 0 && linesField[fromY][fromX+1].nStatus != 2 && linesField[fromY][fromX+1].nParrent == 0) {
      linesField[fromY][fromX+1].nG = linesField[fromY][fromX].nG+10;                                // Parrent node G value + 10
      linesField[fromY][fromX+1].nH = linesField[fromY][fromX].nH+10;                                // Parrent node H value + 10
      linesField[fromY][fromX+1].nF = linesField[fromY][fromX+1].nG + linesField[fromY][fromX+1].nH; // F value is a G and H value sum
      linesField[fromY][fromX+1].nParrent = 4;                                                       // Parrent is on the left side
      linesField[fromY][fromX+1].nStatus = 1;                                                        // Open node
      if (fromY == toY && fromX+1 == toX) {
        ret = true;
      }
    }
  }
  if (fromX != 0) { // Loot at the left
    if (linesField[fromY][fromX-1].nColor == 0 && linesField[fromY][fromX-1].nStatus != 2 && linesField[fromY][fromX-1].nParrent == 0) {
      linesField[fromY][fromX-1].nG = linesField[fromY][fromX].nG+10;                                // Parrent node G value + 10
      linesField[fromY][fromX-1].nH = linesField[fromY][fromX].nH+10;                                // Parrent node H value + 10
      linesField[fromY][fromX-1].nF = linesField[fromY][fromX-1].nG + linesField[fromY][fromX-1].nH; // F value is a G and H value sum
      linesField[fromY][fromX-1].nParrent = 2;                                                       // Parrent is on the right side
      linesField[fromY][fromX-1].nStatus = 1;                                                        // Open node
      if (fromY == toY && fromX-1 == toX) {
        ret = true;
      }
    }
  }
  if (fromY+1 != LINES_FIELD) { // Look down
    if (linesField[fromY+1][fromX].nColor == 0 && linesField[fromY+1][fromX].nStatus != 2 && linesField[fromY+1][fromX].nParrent == 0) {
      linesField[fromY+1][fromX].nG = linesField[fromY][fromX].nG+10;                                // Parrent node G value + 10
      linesField[fromY+1][fromX].nH = linesField[fromY][fromX].nH+10;                                // Parrent node H value + 10
      linesField[fromY+1][fromX].nF = linesField[fromY+1][fromX].nG + linesField[fromY+1][fromX].nH; // F value is a G and H value sum
      linesField[fromY+1][fromX].nParrent = 1;                                                       // Parrent is higher
      linesField[fromY+1][fromX].nStatus = 1;                                                        // Open node
      if (fromY+1 == toY && fromX == toX) {
        ret = true;
      }
    }
  }
  if (fromY != 0) { // Look up
    if (linesField[fromY-1][fromX].nColor == 0 && linesField[fromY-1][fromX].nStatus != 2 && linesField[fromY-1][fromX].nParrent == 0) {
      linesField[fromY-1][fromX].nG = linesField[fromY][fromX].nG+10;                                // Parrent node G value + 10
      linesField[fromY-1][fromX].nH = linesField[fromY][fromX].nH+10;                                // Parrent node H value + 10
      linesField[fromY-1][fromX].nF = linesField[fromY-1][fromX].nG + linesField[fromY-1][fromX].nH; // F value is a G and H value sum
      linesField[fromY-1][fromX].nParrent = 3;                                                       // Parrent is bellow
      linesField[fromY-1][fromX].nStatus = 1;                                                        // Open node
      if (fromY-1 == toY && fromX == toX) {
        ret = true;
      }
    }
  }
  return ret;
}

static bool_t findPath(int fromX, int fromY, int toX, int toY) {
  bool_t ret, cont;
  ret = false;
  cont = true;
  linesField[fromY][fromX].nG = 0;
  linesField[fromY][fromX].nH = steps(fromX, fromY, toX, toY)*10;
  linesField[fromY][fromX].nF = linesField[fromY][fromX].nG + linesField[fromY][fromX].nH; // Node F value is sum of node G and node H values!
  linesField[fromY][fromX].nParrent = 0;                                                   // No parrent node for source!
  linesField[fromY][fromX].nStatus = 1;                                                    // Open node
  while (cont) {
    getOpenMinNode();
    if (linesOpenMin[0] != -1) {
      linesField[linesOpenMin[0]][linesOpenMin[1]].nStatus = 2; // Close Node
      if (lookAround(linesOpenMin[0], linesOpenMin[1], toX, toY)) {
        ret = true;
        cont = false;
      }
    } else {
      cont = false;
    }
  }
  return ret;
}

static void clearPath(void) {
  int i,j;
  for (i = 0; i < LINES_FIELD; i++) {
    for (j = 0; j < LINES_FIELD; j++) {
      linesField[i][j].nG = 0;
      linesField[i][j].nH = 0;
      linesField[i][j].nF = 0;
      linesField[i][j].nParrent = 0;
      linesField[i][j].nStatus = 0;
    }
  }
}

static void generateNextBalls(void) { // Draws three next balls
  uint8_t i;
  for (i = 0; i < 3; i++ ) linesNextBalls[i] = randomInt(7)+1;
  gdispImageOpenFile(&linesBallImage, linesBalls[linesNextBalls[0]-1]);
  gdispImageDraw(&linesBallImage, ((LINES_FIELD*LINES_CELL_WIDTH)/2)-(LINES_CELL_WIDTH/2)-LINES_CELL_WIDTH+3, (LINES_FIELD*LINES_CELL_HEIGHT)+10, 21, 21, 0, 0);
  gdispImageClose(&linesBallImage);
  gdispImageOpenFile(&linesBallImage, linesBalls[linesNextBalls[1]-1]);
  gdispImageDraw(&linesBallImage, ((LINES_FIELD*LINES_CELL_WIDTH)/2)-(LINES_CELL_WIDTH/2)+3, (LINES_FIELD*LINES_CELL_HEIGHT)+10, 21, 21, 0, 0);
  gdispImageClose(&linesBallImage);
  gdispImageOpenFile(&linesBallImage, linesBalls[linesNextBalls[2]-1]);
  gdispImageDraw(&linesBallImage, ((LINES_FIELD*LINES_CELL_WIDTH)/2)-(LINES_CELL_WIDTH/2)+LINES_CELL_WIDTH+3, (LINES_FIELD*LINES_CELL_HEIGHT)+10, 21, 21, 0, 0);
  gdispImageClose(&linesBallImage);
}

static void drawPath(void) {
  int16_t k[2] = {linesPath.sY, linesPath.sX}; // k holds source coords and will move towards target - aka next parrent location...
  int16_t c[2] = {k[0], k[1]};                 // c holds current location, will use this to clear old ball and detect target location!
  // Lets draw path first...
    gdispImageOpenFile(&linesBallImage, linesBalls[linesField[linesPath.tY][linesPath.tX].nColor-1]);
  while (c[0] != linesPath.tY || c[1] != linesPath.tX) {
    if (linesField[c[0]][c[1]].nParrent == 1) {
      k[0]--;
    }
    if (linesField[c[0]][c[1]].nParrent == 2) {
      k[1]++;
    }
    if (linesField[c[0]][c[1]].nParrent == 3) {
      k[0]++;
    }
    if (linesField[c[0]][c[1]].nParrent == 4) {
      k[1]--;
    }
    gdispFillCircle((k[1]*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), (k[0]*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), 2, linesBallsColors[linesField[linesPath.tY][linesPath.tX].nColor-1]);

//    gdispImageDraw(&linesBallImage, (k[1]*LINES_CELL_WIDTH)+3, (k[0]*LINES_CELL_HEIGHT)+3, 21, 21, 0, 0);
    // store next parrent location to current location
    c[0] = k[0];
    c[1] = k[1];
  }
  gfxSleepMilliseconds(200);
  k[0] = linesPath.sY;
  k[1] = linesPath.sX; // k holds source coords and will move towards target - aka next parrent location...
  c[0] = k[0];
  c[1] = k[1];                 // c holds current location, will use this to clear old ball and detect target locat
  while (c[0] != linesPath.tY || c[1] != linesPath.tX) {
    if (linesField[c[0]][c[1]].nParrent == 1) {
      k[0]--;
    }
    if (linesField[c[0]][c[1]].nParrent == 2) {
      k[1]++;
    }
    if (linesField[c[0]][c[1]].nParrent == 3) {
      k[0]++;
    }
    if (linesField[c[0]][c[1]].nParrent == 4) {
      k[1]--;
    }
    gdispFillCircle((c[1]*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), (c[0]*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black);                                                   // Clear old ball
    gdispImageDraw(&linesBallImage, (k[1]*LINES_CELL_WIDTH)+3, (k[0]*LINES_CELL_HEIGHT)+3, 21, 21, 0, 0);
    // wait 100ms - for slow ball moving
    gfxSleepMilliseconds(100);
    // store next parrent location to current location
    c[0] = k[0];
    c[1] = k[1];
  }
  gdispImageClose(&linesBallImage);
}

static void tellScore(void) {
  char pps_str[12];
  font_t font = gdispOpenFont("DejaVuSans16");
  gdispFillArea(0, 296, 240, 17, Black);
  gdispDrawString(10, 300, "Score: ", font, White);
  uitoa(linesScore, pps_str, sizeof(pps_str));
  gdispDrawString(gdispGetStringWidth("Score: ", font)+10, 300, pps_str, font, White);
  gdispCloseFont(font);
}

static bool_t checkColors(void) {
  bool_t ret, cont;
  int16_t i, stps, cX, cY;
  uint8_t cColor = linesField[linesPath.tY][linesPath.tX].nColor;
  ret = false; // Return variable
  pointCoords linesCoords[4]; // [4 lines (- (0), | (1), \(2), /(3)) (horizontal, vertical, main diagonal and antidiagonal)]
  // init default values in loop
  for (i = 0; i < 4; i++) {
    linesCoords[i].sY =    linesPath.tY;
    linesCoords[i].sX =    linesPath.tX;
    linesCoords[i].tY =    linesPath.tY;
    linesCoords[i].tX =    linesPath.tX;
    linesCoords[i].c = 1;
  }
  // Checking horizontal... right
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if (cX + stps != LINES_FIELD) {
      if (linesField[cY][cX+stps].nColor == cColor) {
        linesCoords[0].c++;
        linesCoords[0].tY = cY;
        linesCoords[0].tX = cX+stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking horizontal... left
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if (cX - stps >= 0) {
      if (linesField[cY][cX-stps].nColor == cColor) {
        linesCoords[0].c++;
        linesCoords[0].sY = cY;
        linesCoords[0].sX = cX-stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking vertical... down
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if (cY + stps != LINES_FIELD) {
      if (linesField[cY+stps][cX].nColor == cColor) {
        linesCoords[1].c++;
        linesCoords[1].tY = cY+stps;
        linesCoords[1].tX = cX;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking vertical... up
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if (cY - stps >= 0) {
      if (linesField[cY-stps][cX].nColor == cColor) {
        linesCoords[1].c++;
        linesCoords[1].sY = cY-stps;
        linesCoords[1].sX = cX;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking main diagonal (\)... down
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if ((cY + stps != LINES_FIELD) && (cX + stps != LINES_FIELD)) {
      if (linesField[cY+stps][cX+stps].nColor == cColor) {
        linesCoords[2].c++;
        linesCoords[2].tY = cY+stps;
        linesCoords[2].tX = cX+stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking main diagonal (\)... up
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if ((cY - stps >= 0) && (cX - stps >= 0)) {
      if (linesField[cY-stps][cX-stps].nColor == cColor) {
        linesCoords[2].c++;
        linesCoords[2].sY = cY-stps;
        linesCoords[2].sX = cX-stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking antidiagonal (/)... down
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if ((cY + stps != LINES_FIELD) && (cX - stps >= 0)) {
      if (linesField[cY+stps][cX-stps].nColor == cColor) {
        linesCoords[3].c++;
        linesCoords[3].tY = cY+stps;
        linesCoords[3].tX = cX-stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  // Checking antidiagonal (/)... up
  cY = linesPath.tY;
  cX = linesPath.tX;
  cont = true;
  stps = 1;
  while (cont) {
    if ((cY - stps >= 0) && (cX + stps != LINES_FIELD)) {
      if (linesField[cY-stps][cX+stps].nColor == cColor) {
        linesCoords[3].c++;
        linesCoords[3].sY = cY-stps;
        linesCoords[3].sX = cX+stps;
      } else {
        cont = false;
      }
    } else {
      cont = false;
    }
    stps++;
  }
  //got info about all directions, now, lets see if we need to remove balls...
  unsigned int points = 0;
  unsigned int multiplier = 0;
  // Remove horizontal balls if needed
  if (linesCoords[0].c > 4) {
    points += linesCoords[0].c*2;
    multiplier++;
    for (i = 0; i < linesCoords[0].c; i++) {
      linesField[linesCoords[0].sY][linesCoords[0].sX+i].nColor = 0;
      gdispFillCircle(((linesCoords[0].sX+i)*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), (linesCoords[0].sY*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black); // Clear old ball
      gfxSleepMilliseconds(100);
    }
    linesEmptyNodes += linesCoords[0].c-1; // We don't count final ball (the one that completed 5 or more in a row), we need to count final ball only once (hence -1)!
  }
  // Remove vertical balls if needed
  if (linesCoords[1].c > 4) {
    points += linesCoords[1].c*2;
    multiplier++;
    for (i = 0; i < linesCoords[1].c; i++) {
      linesField[linesCoords[1].sY+i][linesCoords[1].sX].nColor = 0;
      gdispFillCircle((linesCoords[1].sX*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), ((linesCoords[1].sY+i)*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black); // Clear old ball
      gfxSleepMilliseconds(100);
    }
    linesEmptyNodes += linesCoords[1].c-1; // We don't count final ball (the one that completed 5 or more in a row), we need to count final ball only once (hence -1)!
  }
  // Remove main diagonal balls if needed
  if (linesCoords[2].c > 4) {
    points += linesCoords[2].c*2;
    multiplier++;
    for (i = 0; i < linesCoords[2].c; i++) {
      linesField[linesCoords[2].sY+i][linesCoords[2].sX+i].nColor = 0;
      gdispFillCircle(((linesCoords[2].sX+i)*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), ((linesCoords[2].sY+i)*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black); // Clear old ball
      gfxSleepMilliseconds(100);
    }
    linesEmptyNodes += linesCoords[2].c-1; // We don't count final ball (the one that completed 5 or more in a row), we need to count final ball only once (hence -1)!
  }
  // Remove antidiagonal balls if needed
  if (linesCoords[3].c > 4) {
    points += linesCoords[3].c*2;
    multiplier++;
    for (i = 0; i < linesCoords[3].c; i++) {
      linesField[linesCoords[3].sY+i][linesCoords[3].sX-i].nColor = 0;
      gdispFillCircle(((linesCoords[3].sX-i)*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), ((linesCoords[3].sY+i)*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black); // Clear old ball
      gfxSleepMilliseconds(100);
    }
    linesEmptyNodes += linesCoords[3].c-1; // We don't count final ball (the one that completed 5 or more in a row), we need to count final ball only once (hence -1)!
  }
  if (multiplier != 0) {
    if (multiplier > 1) {
      points = points * multiplier;
    }
    if (multiplier == 1) {
      multiplier = 0;
    }
    linesScore += points+multiplier;
    ret = true;
    linesEmptyNodes++; // add one more here - this is final ball (the one that completed 5 or more in a row)
    tellScore();
  }
  return ret;
}

static void addBalls(void) {
  int16_t emptyNodes,x,y;
  if (linesEmptyNodes <= 2) emptyNodes = linesEmptyNodes; else emptyNodes = 3;
  if (emptyNodes > 0) { // need to add balls
    while (emptyNodes != 0) {
      x = randomInt(LINES_FIELD);
      y = randomInt(LINES_FIELD);
      if (linesField[y][x].nColor == 0) {
        linesField[y][x].nColor = linesNextBalls[emptyNodes-1];
        gdispImageOpenFile(&linesBallImage, linesBalls[linesNextBalls[emptyNodes-1]-1]);
        gdispImageDraw(&linesBallImage, (x*LINES_CELL_WIDTH)+3, (y*LINES_CELL_HEIGHT)+3, 21, 21, 0, 0);
        gdispImageClose(&linesBallImage);
        emptyNodes--;
        linesEmptyNodes--;
        linesPath.tX = x;
        linesPath.tY = y;
        checkColors();
        gfxSleepMilliseconds(100);
      }
    }
    generateNextBalls();
  } else { //No room for new balls, game over! :D
    linesGameOver = true;
  }
}

static void printCantMoveThere(void) {
  font_t font = gdispOpenFont("DejaVuSans16");
  gdispDrawString((240-gdispGetStringWidth("Can't move there!", font))/2, 270, "Can't move there!", font, Red);
  gdispCloseFont(font);
  linesPreviousMessageTime = gfxSystemTicks();
  linesMessageShown = true;
}

static void goToTarget(void) {
  linesField[linesPath.tY][linesPath.tX].nColor = linesField[linesPath.sY][linesPath.sX].nColor;             // Set target color the same as source color
  linesField[linesPath.sY][linesPath.sX].nColor = 0;                                                         // Set source color to black
  bool_t pathAvailable = findPath(linesPath.tX, linesPath.tY, linesPath.sX, linesPath.sY);
  if (pathAvailable) {
    drawPath();
    gdispDrawBox(linesPath.sX*LINES_CELL_WIDTH, linesPath.sY*LINES_CELL_HEIGHT, LINES_CELL_WIDTH+1, LINES_CELL_HEIGHT+1, Gray);
    bool_t linesRemoved = checkColors();
    if (!linesRemoved) addBalls();
    linesPath.sY = -1;
    linesPath.sX = -1;
  } else {
    // Can't go there
    printCantMoveThere();
    linesField[linesPath.sY][linesPath.sX].nColor = linesField[linesPath.tY][linesPath.tX].nColor; // Reset source/target colors
    linesField[linesPath.tY][linesPath.tX].nColor = 0;                                             // Set target color to black
  }
  clearPath();
  linesPath.tY = -1;
  linesPath.tX = -1;
}

static msg_t thdLines(void *arg) {
  (void)arg;
  uint8_t x,y;
  while (!linesGameOver) {
    srand(gfxSystemTicks());
    if (linesMessageShown && gfxSystemTicks() - linesPreviousMessageTime >= MS2ST(linesMessageTimeout)) {
      linesMessageShown = false;
      gdispFillArea(0, 270, 240, 17, Black);
    }
    ginputGetMouseStatus(0, &ev);
    if (ev.current_buttons & GINPUT_MOUSE_BTN_LEFT) {
      x = ev.x/LINES_CELL_WIDTH;
      y = ev.y/LINES_CELL_WIDTH;
      if (x < LINES_FIELD && y < LINES_FIELD) {
        if (linesField[y][x].nColor == 0) { // tap on empty field
          // let's add target, if source is selected before!
          if (linesPath.sY != -1) { // -1 means no source selected
            // adding target coords
            linesPath.tY = y;
            linesPath.tX = x;
            goToTarget();
          }
        } else {
          // adding source coords
          if (linesPath.sY != -1 && linesPath.sX != -1) gdispDrawBox(linesPath.sX*LINES_CELL_WIDTH, linesPath.sY*LINES_CELL_HEIGHT, LINES_CELL_WIDTH+1, LINES_CELL_HEIGHT+1, Gray);
          gdispDrawBox(x*LINES_CELL_WIDTH, y*LINES_CELL_HEIGHT, LINES_CELL_WIDTH+1, LINES_CELL_HEIGHT+1, White);
          linesPath.sY = y;
          linesPath.sX = x;
        }
      }
      while (ev.current_buttons & GINPUT_MOUSE_BTN_LEFT) { // Wait until release
        ginputGetMouseStatus(0, &ev);
        gfxSleepMilliseconds(100);
      }
    }
    if (linesEmptyNodes == 0) linesGameOver = true;
  }
  return (msg_t)0;
}

static void clearField(void) {
  int16_t i,j;
  for (i = 0; i < LINES_FIELD; i++) {
    for (j = 0; j < LINES_FIELD; j++) {
      gdispFillCircle((j*LINES_CELL_WIDTH)+(LINES_CELL_WIDTH/2), (i*LINES_CELL_HEIGHT)+(LINES_CELL_HEIGHT/2), LINES_BALL_RADIUS, Black);
    }
    gfxSleepMilliseconds(50);
  }
}

static void printGameOver(void) {
  if (linesGameOver) {
    font_t font = gdispOpenFont("DejaVuSans16");
    if (linesMessageShown) {
      linesMessageShown = false;
      gdispFillArea(0, 270, 240, 17, Black); // In case there is "Can't move" message shown - clear it and print "Game Over!"
    }
    gdispDrawString((240-gdispGetStringWidth("Game Over!", font))/2, 270, "Game Over!", font, White);
    gdispCloseFont(font);
  } else {
    gdispFillArea(0, 270, 240, 17, Black);
  }
}

static void initField(void) {
  int i,j;
  for (i = 0; i < LINES_FIELD; i++) {
    for (j = 0; j < LINES_FIELD; j++) {
      linesField[i][j].nColor = 0; // color index - 0 means empty
      linesField[i][j].nG = 0;
      linesField[i][j].nH = 0;
      linesField[i][j].nF = 0;
      linesField[i][j].nParrent = 0;
      linesField[i][j].nStatus = 0;
    }
  }
  linesEmptyNodes = LINES_FIELD*LINES_FIELD;
  linesScore = 0;
  generateNextBalls();
  linesGameOver = false;
  printGameOver();
  addBalls();
  tellScore();
  linesPath.sY = -1;
  linesPath.sX = -1;
  linesPath.tY = -1;
  linesPath.tX = -1;
}

void linesStart(void) {
  initField();
  //Thread *linesThd = chThdCreateStatic(waLines, sizeof(waLines), NORMALPRIO, thdLines, NULL);
  //Thread *linesThd = gfxThreadCreate(waLines, sizeof(waLines), NORMALPRIO, thdLines, NULL);//
  gfxThreadHandle linesThd = gfxThreadCreate(0, 10240, NORMALPRIO, thdLines, 0); 
  while (!linesGameOver) {
    gfxSleepMilliseconds(1000);
  }
  printGameOver();
  //chThdTerminate(linesThd);
  gfxThreadClose(linesThd);
  clearField();
}

void linesInit(void) {
  int i;
  initRng();
  for ( i = 0; i <= LINES_FIELD; i++) {
    gdispDrawLine(i*LINES_CELL_WIDTH, 0, i*LINES_CELL_WIDTH, LINES_FIELD*LINES_CELL_HEIGHT, Gray);// Vertical lines
    gdispDrawLine(0, i*LINES_CELL_HEIGHT, LINES_FIELD*LINES_CELL_WIDTH, i*LINES_CELL_HEIGHT, Gray);// Horizontal lines
  }
  font_t font = gdispOpenFont("DejaVuSans16");
  gdispDrawString(30, (LINES_FIELD*LINES_CELL_HEIGHT)+12, "Next", font, White);
  gdispCloseFont(font);
}
